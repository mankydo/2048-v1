using Godot;
using System;

public class ResizeChildToFitScreen : Node2D
{

    [Export]
	NodePath AreaStartPoint2D;
	[Export]
	NodePath AreaEndPoint2D;

	private Position2D _AreaStartPoint2D;
	private Position2D _AreaEndPoint2D;

    [Export]
	NodePath ChildNode2DToResize;
    private Node2D _ChildNode2DToResize;

    [Export(PropertyHint.Range, "0,200")]
    private float _MarginX = 20;

    Vector2 _ChildScaleInitial, _ChildScaleCurrent;


    public override void _Ready()
    {
        if(AreaStartPoint2D != null) 
		{
			Type type = GetNode(AreaStartPoint2D).GetType();

			if(type == typeof(Position2D))
				_AreaStartPoint2D = (Position2D)GetNode(AreaStartPoint2D);
			else
				GD.Print("!!!See no start position to use for.");

			GD.Print("got a start position 2d to operate: " + type.Name);
		} else
			GD.Print("!!!See no start position object to use for.");

        if(_AreaStartPoint2D != null)
        {
            float posEndX = _AreaStartPoint2D.GlobalPosition.x;
            GD.Print("Start position = " + posEndX);
        }
        
        if(AreaEndPoint2D != null) 
		{
			Type type = GetNode(AreaEndPoint2D).GetType();

			if(type == typeof(Position2D))
				_AreaEndPoint2D = (Position2D)GetNode(AreaEndPoint2D);
			else
				GD.Print("!!!See no start position to use for.");

			GD.Print("got a start position 2d to operate: " + type.Name);
		} else
			GD.Print("!!!See no start position object to use for.");

        if(_AreaEndPoint2D != null)
        {
            float posEndX = _AreaEndPoint2D.GlobalPosition.x;
            GD.Print("End position = " + posEndX);
        }

        if(ChildNode2DToResize != null) 
		{
			Type type = GetNode(ChildNode2DToResize).GetType();
            GD.Print("child node type is: " + type.Name + " and it's basetype is " + type.BaseType.Name);

			if(type == typeof(Node2D) || type.BaseType == typeof(Node2D) )
				_ChildNode2DToResize = (Node2D)GetNode(ChildNode2DToResize);
			else
				GD.Print("!!!See no Child Node2D to use for.");
		} else
			GD.Print("!See no Child Node2D to use for.");

        if(_ChildNode2DToResize != null)
        {
            _ChildScaleInitial = _ChildNode2DToResize.Scale;
            _ChildScaleCurrent = _ChildScaleInitial;
        }

    }

  public override void _Process(float delta)
  {
        Vector2 screenSize = GetViewport().Size;
//        GD.Print("Screen size is: " + screenSize);

        if(_AreaStartPoint2D != null && _ChildNode2DToResize != null)
        {
            float posEndX = _AreaEndPoint2D.GlobalPosition.x + _MarginX;
            if(posEndX >= screenSize.x)
            {
                float ratio = (screenSize.x / posEndX) * _ChildScaleInitial.x;
                _ChildScaleCurrent.x =  Math.Min(ratio, _ChildScaleInitial.x);
                _ChildScaleCurrent.y = _ChildScaleCurrent.x;

                _ChildNode2DToResize.Scale = _ChildScaleCurrent;

//                GD.Print("new scale is: " + _ChildScaleCurrent);
            }
        }


  }
}
