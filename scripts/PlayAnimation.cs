using Godot;
using System;

public class PlayAnimation: Node2D {
    [Export]
    NodePath AnimationPlayerToOperateOnPath;
    [Export]
    string _animationNameToPlayAtStart = "";

    private AnimationPlayer _animPlayer;

    public override void _Ready()
    {
        if(AnimationPlayerToOperateOnPath != null) 
        {
            Type type = GetNode(AnimationPlayerToOperateOnPath).GetType();

            if(type != null)
                _animPlayer = (AnimationPlayer)GetNode(AnimationPlayerToOperateOnPath);
            else
                GD.Print("!!!See no Game Controller to use for.");

//            GD.Print("got an animation player to operate: " + type.Name);
        } 
        if(_animPlayer != null)
        {
            PlayDefaultAnimation();
        }
    }

    public void PlayAnimationName(string animName)
    {
        if(_animPlayer != null && animName != "")
        {
            _animPlayer.Play(animName);
        }
    }

    public void PlayDefaultAnimation()
    {
       PlayAnimationName(_animationNameToPlayAtStart);
    }
}
