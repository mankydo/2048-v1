
//public delegate void DisplayGameField(GameField gameField, float durationInSeconds);
public delegate void NoRoomLeftOnAField();

public enum InputGameEvent{left, right, up, down, noswipe};

public interface IGameLogic
{
//    event DisplayGameField DisplayGameFieldEvent;
    event NoRoomLeftOnAField NoRoomLeftOnAFieldEvent;

    void GameInit(GameField gameField);

    void StartNewGame();         //Start a new game
    void CreateNewCell();   //Generate new cell in a random place
    void PrepareFieldForNextTurn();        //Clear states of cells
    void ProcessUserInput(InputGameEvent inputEvent, out bool hasAnyCellMoved);    //Mark cells according to the game rules and change their values
}