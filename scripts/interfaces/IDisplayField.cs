public interface IDisplayField
{
    void Init(IGameLogic gameLogic);
    void DisplayGameField(GameField gameField, float durationInSeconds);

    void ResetDisplay();
}