public interface IStorable
{
    string GetParameter(string name);
}