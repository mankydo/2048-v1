public interface IGameController
{
    void InvokeSignal(string signalName);
}