using Godot;
using System;

public class InputController: Node {

    [Export]
    NodePath GameControllerNodeToOperateOnPath;
    [Export(PropertyHint.Range, "0,1")]
    private float _SwipeDistance = .05f;
    [Export(PropertyHint.Range, "0,2")]
    private float _SwipeTimeInSeconds = .25f;

    private GameController _gameController;
    
    private ulong _startTouchTime = 0;
    private Vector2 _startTouchPosition;
    public override void _Ready()
    {
        if(GameControllerNodeToOperateOnPath != null) 
        {
            Type type = GetNode(GameControllerNodeToOperateOnPath).GetType();

            if(type != null)
                _gameController = (GameController)GetNode(GameControllerNodeToOperateOnPath);
            else
                GD.Print("!!!See no Game Controller to use for.");

            GD.Print("got a GameLogic to operate: " + type.Name);
        } 
    }
    
    public override void _UnhandledInput(InputEvent keyevent)
    {
        string eventToProceed = "";

        if(Input.IsActionPressed("ui_left"))
        {
            GD.Print("left is pressed");
            eventToProceed = "left";
        }
        if(Input.IsActionPressed("ui_right"))
        {
            GD.Print("right is pressed");
            eventToProceed = "right";
        }
                if(Input.IsActionPressed("ui_up"))
        {
            GD.Print("up is pressed");
            eventToProceed = "up";
        }
                if(Input.IsActionPressed("ui_down"))
        {
            GD.Print("down is pressed");
            eventToProceed = "down";
        }

        if( keyevent is InputEventScreenTouch)
        {
//            GD.Print("I Got a touch event!!");
            InputEventScreenTouch event1 = (InputEventScreenTouch)keyevent;
            if(event1.Pressed)
            {
                _startTouchPosition = event1.Position;
                _startTouchTime = OS.GetTicksMsec(); 
//                GD.Print("swipe started at " + _startTouchTime);
            }
            else if(!event1.Pressed && _startTouchTime > 0 && (OS.GetTicksMsec() - _startTouchTime) < (ulong)(_SwipeTimeInSeconds * 1000) )
            {
                string swipeDir = DetectSwipe(_startTouchPosition, event1.Position, _SwipeDistance);
                GD.Print("swipe detected: " + swipeDir);
                _startTouchTime = 0;

                if(swipeDir != "")
                    eventToProceed = swipeDir;
            }
        

            //Vector2 screenSize = GetViewport().Size;
            //GD.Print(screenSize + " : Touch coords are: " + _startTouchPosition);

        }

        if(_gameController !=null && eventToProceed != "") 
        {
            _gameController.GameInput(eventToProceed);
            
            //should be a timeout for an animation to play
//            _gameLogic.CreateNewCell();
        }
    }

    private string DetectSwipe(Vector2 previousPosition, Vector2 newPosition, float minDistanceForSwipe)
    {
        string result = "";
        
        Vector2 delta = newPosition - previousPosition;
        GD.Print("delta is: " + delta);
        if( Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
        {
            if(delta.x > 0)
                result = "right";
            else
                result = "left";
        }
        else 
        {
            if(delta.y > 0)
                result = "down";
            else
                result = "up";
        }

        return result;
    }
}