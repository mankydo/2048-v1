using Godot;
using System;

public class DisplayFieldConsole : Node, IDisplayField
{
    [Export]
    private int _SizeX = 4;
    [Export]
    private int _SizeY = 4;

  //  [Export]
    NodePath nodeToOperateOnPath;
    private IGameLogic _gameLogic;

    public override void _Ready()
    {
        if(nodeToOperateOnPath != null) 
        {
            IGameLogic gameLogic = (IGameLogic)GetNode(nodeToOperateOnPath);
            GD.Print("got a node to operate ");

            Init(gameLogic);
        }
        
    }
    public void Init(IGameLogic gameLogic)
    {
        _gameLogic = gameLogic;
//        _gameLogic.DisplayGameFieldEvent += DisplayGameField;
    }

    public void ResetDisplay()
    {
//        _gameLogic.DisplayGameFieldEvent -= DisplayGameField;
    }
    public void DisplayGameField(GameField gameField, float durationInSeconds)
    {
        for(int y = 0; y <= _SizeY - 1; y++)
        {
            string outputRow = "y: " + y + ":: ";
            for(int x = 0; x <= _SizeX - 1; x++)
            {
                int value = gameField.GetCellValueByCoords(x, y);
                value = value < 0 ? 0 : value;
                outputRow += "--" + value;
                
                Cell.Status stat = gameField.GetCellStatus(gameField.GetNotNullCellIdByCoords(x,y));
                string status = stat == Cell.Status.appeared ? "+" :  
                    stat == Cell.Status.moved ? "*" :  
                    stat == Cell.Status.changed ? "^" :
                    stat == Cell.Status.movedAndDeleted ? "#" :  
                    stat == Cell.Status.deleted ? "%" : "";
                
                outputRow += status;
            }
            GD.Print(outputRow);
        } 
    }

    public override void _ExitTree()
    {
        GD.Print("I'm dying");
//        if(_gameLogic != null)
//            _gameLogic.DisplayGameFieldEvent -= DisplayGameField;
    }
}