using Godot;
using System;

using Utilities;

public class TestMoveUtility : Node2D
{

    [Export]
    private float timeToMoveInSeconds = 10;

    [Export]
    NodePath nodeToMove;
    private Node2D nodeToMove1;

    public override void _Ready()
    {

        if(nodeToMove != null)
        {
            Type type = GetNode(nodeToMove).GetType();

            GD.Print("Type is: " + type);

    //        if(type == typeof(Node2D))
                nodeToMove1 = (Node2D)GetNode(nodeToMove);
        }

        GD.Print("kinda starting moving");

        if(nodeToMove1 != null)
        {
            GD.Print("Totally starting moving");
            Utilities.NodeUtilities.MoveNode2D(nodeToMove1, new Vector2(1000, 500), timeToMoveInSeconds);
        }
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
