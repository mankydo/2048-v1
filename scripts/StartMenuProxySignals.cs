using Godot;
using System;

public class StartMenuProxySignals : Node2D
{
    [Signal]
    delegate void ButtonPressed();

    [Export]
    private string _HideName = "dissapear";
    [Export]
    private string _ShowName = "appear";

     [Export]
    NodePath AnimationPlayerToOperateOnPath;
    private AnimationPlayer _animPlayer;

    public override void _Ready()
    {
        if(AnimationPlayerToOperateOnPath != null) 
        {
            Type type = GetNode(AnimationPlayerToOperateOnPath).GetType();

            if(type != null)
                _animPlayer = (AnimationPlayer)GetNode(AnimationPlayerToOperateOnPath);
            else
                GD.Print("!!!See no animation Controller to use for.");

//            GD.Print("got an animation player to operate: " + type.Name);
        } 
    }

    public void EmitPress()
    {
        GD.Print("button pressed    ");
        EmitSignal("ButtonPressed");
    }

    public void ShowMenu(){
        GD.Print("SHowing a window");
        if(_animPlayer!= null){
            _animPlayer.Play(_ShowName);
            GD.Print("shoooowin");
        }
    }

    public void HideMenu()
    {
        if(_animPlayer!= null)
            _animPlayer.Play(_ShowName);
    }

}
