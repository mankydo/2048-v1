using Godot;
using System;

public class TestScript : Node
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    [Export]
    NodePath nodeToOperateOnPath;
    Node nodeToOperateOn;
    [Export]
    string _NodeName = "Button1";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GD.Print("hello!");
        if(nodeToOperateOnPath != null) 
        {
            nodeToOperateOn = GetNode(nodeToOperateOnPath);


            GD.Print(nodeToOperateOn.Name);

            foreach(Node child in nodeToOperateOn.GetChildren())
            {
                GD.Print("Child name is: " + child.Name);
                foreach(Node child2 in child.GetChildren())
                {
                    if(child2.Name == _NodeName)           
                    {  
                        Button buttonToChange = (Button)child2;
                        buttonToChange.Text = "not so testing anymore";   
                    }
                }
            }     
        }
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
