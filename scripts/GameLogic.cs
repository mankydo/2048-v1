using Godot;
using System;
using System.Collections.Generic;

public class GameLogic: Node, IGameLogic {

    private int _sizeX = 4;
    private int _sizeY = 4;


//    public event DisplayGameField DisplayGameFieldEvent;
    public event NoRoomLeftOnAField NoRoomLeftOnAFieldEvent;
    private GameField _GameField;
    
    public void GameInit(GameField gameField)
    {
        _sizeX = gameField.sizeX;
        _sizeY = gameField.sizeY;
        
        _GameField = gameField;
        _GameField.ClearField();
    }


    public void StartNewGame()
    {
        _GameField.ClearField(); 
        PrepareFieldForNextTurn();
        CreateNewCell();
    }

    //Generate new cell in a random place
    public void CreateNewCell()
    {
        PrepareFieldForNextTurn();

        int newX, newY;
        int checkForExistingCell = 0;
        do
        {
            Random rnd = new Random();
            newX = rnd.Next(0, _GameField.sizeX);
            newY = rnd.Next(0, _GameField.sizeY);

            checkForExistingCell = _GameField.GetCellValueByCoords(newX, newY);
        } while(_GameField.GetNumberOfNotNullCells() < _GameField.sizeX * _GameField.sizeY && checkForExistingCell > 0);

        if(_GameField.GetNumberOfNotNullCells() < _GameField.sizeX * _GameField.sizeY )
        {
            int newCellId = _GameField.CreateCell();
            _GameField.SetCellCoords(newCellId, newX, newY);
            _GameField.SetCellValue(newCellId, 2);
            _GameField.SetCellStatus(newCellId, Cell.Status.appeared);

            GD.Print("New cell created, id: " + newCellId + " and a number of non-null cells is: " + _GameField.GetNumberOfNotNullCells());
            GD.Print("Coords if a new cell are: " + newX + ", " + newY);
        } 
        else {
            NoRoomLeftOnAFieldEvent?.Invoke();
            GD.Print("Can't add a new cell!");
        }
    }

    //Mark cells according to the game rules and change their values
    public void ProcessUserInput(InputGameEvent inEvent, out bool hasAnyCellMoved)
    {
        PrepareFieldForNextTurn();

        GD.Print("Event recieved: " + inEvent.ToString());
        Vector2 moveVector = Vector2.Zero;

        //  Define a vector to move in a given direction
        if(inEvent == InputGameEvent.down)
            moveVector = Vector2.Down;
        else if(inEvent == InputGameEvent.left)
            moveVector = Vector2.Left;
        else if(inEvent == InputGameEvent.right)
            moveVector = Vector2.Right;
        else if(inEvent == InputGameEvent.up)
            moveVector = Vector2.Up;


        GD.Print("move vector is: " + moveVector.ToString());    


        Action<GameField, int, int, Vector2> MoveCellInDirection = null;
        MoveCellInDirection = (gameField, cellx, celly, direction) => 
        {
            int cellId = gameField.GetNotNullCellIdByCoords(cellx, celly);
            Vector2 newCoords = new Vector2(cellx, celly) + direction;
            int nextCellId = gameField.GetNotNullCellIdByCoords(newCoords);

//            GD.Print("Found a cell with a value of " + gameField.GetCellValueById(cellId) + ", Old coords are: " + cellx + "," + celly + " and new coords are: " + newCoords)

            if(gameField.CheckIfCellIdExists(nextCellId))
            {
                if(gameField.GetCellValueById(cellId) == gameField.GetCellValueById(nextCellId))
                {
                    int newValue = gameField.GetCellValueById(cellId) + gameField.GetCellValueById(nextCellId);
                    gameField.SetScore(gameField.GetScore() + newValue);

                    gameField.SetCellValue(nextCellId, newValue );

                    if(gameField.GetCellStatus(nextCellId) == Cell.Status.moved)
                    {
//                        GD.Print("Moved and changed");
                        gameField.SetCellStatus(nextCellId, Cell.Status.movedAndChanged);
                    }
                    else
                        gameField.SetCellStatus(nextCellId, Cell.Status.changed);

                    gameField.SetCellValue(cellId, 0);
                    gameField.SetCellCoords(cellId, newCoords);
                    gameField.SetCellStatus(cellId, Cell.Status.movedAndDeleted);

//                    GD.Print("Set cell:" + nextCellId + " in " + newCoords + " to value " + gameField.GetCellValueById(nextCellId));
                } 
            } 
            else 
            {
                if(newCoords.x >= 0 && newCoords.x < _GameField.sizeX && newCoords.y >= 0 && newCoords.y < _GameField.sizeY)
                {
                    gameField.SetCellCoords(cellId, newCoords);
                    gameField.SetCellStatus(cellId, Cell.Status.moved);

//                    GD.Print("Moved cell:" + cellId + " to coords: " + newCoords);

                    MoveCellInDirection(gameField, (int)Mathf.RoundToInt(newCoords.x), (int)Mathf.RoundToInt(newCoords.y), direction);
                }
            }
        };

        int sizeMax = _GameField.sizeX;
        int invertDirection = 0;

        GD.Print("Start/stop counts are: " + sizeMax + " / " + 0 + " and direction is: " + invertDirection);

        int x = 0, y = 0;

        for(int xLoop = 0; xLoop < _GameField.sizeX;)
        {
            x = moveVector.x < 0 ? xLoop : (_GameField.sizeX - 1) - xLoop;
            xLoop++;
            for(int yLoop = 0; yLoop < _GameField.sizeY;)
            {
                y = moveVector.y < 0 ? yLoop : (_GameField.sizeY - 1) - yLoop;
                yLoop++;

                int cellId = -1;

                List<int> cellsInCoords = _GameField.GetCellsIdByCoords(x, y);
                foreach(int c in cellsInCoords){
                    if(_GameField.GetCellStatus(c) != Cell.Status.deleted || _GameField.GetCellStatus(c) != Cell.Status.movedAndDeleted)
                        cellId = c;
                        break;
                }
//                int cellId = _GameField.GetCellIdByCoords(x, y);
//                GD.Print("Coords: " + x + "," + y);

                if(_GameField.CheckIfCellIdExists(cellId))
                {
                    MoveCellInDirection(_GameField, x, y, moveVector);
                }
            }
        }

        //check if any of cells moved and if they are spawn a new cell, overwise do nothing

        hasAnyCellMoved = false;

        for(int x1 = 0; x1 < _GameField.sizeX; x1++)
        {
            for(int y1 = 0; y1 < _GameField.sizeY; y1++)
            {
                foreach(int cellId in _GameField.GetCellsIdByCoords(x1,y1)) 
                {
                    if(_GameField.GetCellStatus(cellId) == Cell.Status.moved 
                        || _GameField.GetCellStatus(cellId) == Cell.Status.movedAndChanged
                        || _GameField.GetCellStatus(cellId) == Cell.Status.movedAndDeleted)
                    {
                        hasAnyCellMoved = true;
                    } 
                }
            }
        } 

        if(!AreAnyMovesLeft())
            NoRoomLeftOnAFieldEvent?.Invoke();
    }

    public void PrepareFieldForNextTurn()
    {
        //cleaning up previous turn deleted cells
        _GameField.MarkAsDeletedNullCells();
        _GameField.ClearDeletedCells();

        //clearing statuses so user input processing would start with from a clean slate
        for(int x1 = 0; x1 < _GameField.sizeX; x1++)
        {
            for(int y1 = 0; y1 < _GameField.sizeY; y1++)
            {
                foreach(int cellId in _GameField.GetCellsIdByCoords(x1,y1)) 
                {
                    _GameField.SetCellStatus(cellId, Cell.Status.none); 
                }
            }
        } 
    }

    public bool AreAnyMovesLeft()
    {
        bool areAnyMovesLeft = false;

        Func<GameField, int, int, int, bool> CheckIfCellCanMove = null;
        CheckIfCellCanMove = (gameField, x, y, ValueToCheckAgainst) =>
        {
            bool resultF = false;
            if(gameField.GetNonDeletedCellsIdByCoords(x, y).Count == 0)
            {
                resultF = true;
            } 
            else
            {
                foreach(int cellIdF in gameField.GetNonDeletedCellsIdByCoords(x,y))
                {
                    if(gameField.GetCellValueById(cellIdF) == ValueToCheckAgainst)
                    {
                        resultF = true;
                    }
                }
            }
            return resultF;
        };

        for(int x = 0; x < _GameField.sizeX; x++)
        {
            for(int y = 0; y < _GameField.sizeY; y++)
            {
                if(_GameField.GetNonDeletedCellsIdByCoords(x, y).Count == 0)
                {
                    areAnyMovesLeft = true;
                } 
                else
                {
                    foreach(int cellId in _GameField.GetNonDeletedCellsIdByCoords(x,y)) 
                    {
                        if(y + 1 < _GameField.sizeY)
                            areAnyMovesLeft = areAnyMovesLeft || CheckIfCellCanMove(_GameField, x, y + 1, _GameField.GetCellValueById(cellId));
                        if(x + 1 < _GameField.sizeX)
                            areAnyMovesLeft = areAnyMovesLeft || CheckIfCellCanMove(_GameField, x + 1, y, _GameField.GetCellValueById(cellId));
                    }
                }
            }
        }

        return areAnyMovesLeft;
    }
}