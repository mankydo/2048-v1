using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
//using Utilities;
using UtilitiesThreadSafeAll;

public class DisplayGridDataCell 
{
	public int cellId;
	public Node2D attachedTilePrefab;
	public int x;
	public int y;
	public int value;
}

public class DisplayGridSimple2D : Node2D, IDisplayField 
{

	[Export]
	NodePath AreaStartPoint2D;
	[Export]
	NodePath AreaEndPoint2D;
	[Export]
	NodePath LabelForScore;
	Label _labelScoreToUse;

	[Export]
	private int _SizeX = 4;
	[Export]
	private int _SizeY = 4;
	[Export]
	private float _scaleOfTiles = 1;

	[Export]
	private Texture _backTexture;

	[Export]
	private List<PackedScene> _ListOfSpritesForTiles = new List<PackedScene>();
	private Dictionary<int, PackedScene> _ValueTextureMatch = new Dictionary<int, PackedScene>();
	private Dictionary<int, DisplayGridDataCell> _DisplayData = new Dictionary<int, DisplayGridDataCell>();

	private Position2D _AreaStartPoint2D;
	private Position2D _AreaEndPoint2D;
	private IGameLogic _gameLogic;
	private Node2D _BackgroundContainerNode;
	private Node2D _ForegroundContainerNode;

	private int _StartPositionX = 0;
	private int _StartPositionY = 0;
	private int _OffsetPositionX = 0;
	private int _OffsetPositionY = 0;



	public override void _Ready()
	{
		GD.Print("Starting to display");

		if(AreaStartPoint2D != null) 
		{
			Type type = GetNode(AreaStartPoint2D).GetType();

			if(type == typeof(Position2D))
				_AreaStartPoint2D = (Position2D)GetNode(AreaStartPoint2D);
			else
				GD.Print("!!!See no start position to use for.");

			GD.Print("got a start position 2d to operate: " + type.Name);
		} else
			GD.Print("!!!See no start position object to use for.");

		if(AreaEndPoint2D != null) 
		{
			Type type = GetNode(AreaEndPoint2D).GetType();

			if(type == typeof(Position2D))
				_AreaEndPoint2D = (Position2D)GetNode(AreaEndPoint2D);
			else
				GD.Print("!!!See no end position to use for.");

			GD.Print("got a end position 2d to operate: " + type.Name);
		} else
			GD.Print("!!!See no end position object to use for.");

		if(LabelForScore != null) 
		{
			Type type = GetNode(LabelForScore).GetType();

			if(type == typeof(Label))
				_labelScoreToUse = (Label)GetNode(LabelForScore);
			else
				GD.Print("!!!See no start label to use for.");

			GD.Print("got a score label 2d to operate: " + type.Name);
		} 

		 for(int i = 0; i < _ListOfSpritesForTiles.Count; i++)
		{
			int value = Mathf.RoundToInt(Mathf.Pow(2, (float)i + 1));
			_ValueTextureMatch[value] = _ListOfSpritesForTiles[i];
			GD.Print(value + ": " + _ListOfSpritesForTiles[i].ResourcePath);
		}
	}

	public void Init(IGameLogic gameLogic)
	{
		//somehow it isn't executed when not in the gamecontroller loop(should read something about an eorder of execution)
		_Ready();

		_gameLogic = gameLogic;
//        _gameLogic.DisplayGameFieldEvent += DisplayGameField;

		_StartPositionX = (int)_AreaStartPoint2D.GlobalPosition.x;
		_StartPositionY = (int)_AreaStartPoint2D.GlobalPosition.y;

		int endPositionX = (int)_AreaEndPoint2D.GlobalPosition.x;
		int endPositionY = (int)_AreaEndPoint2D.GlobalPosition.y;


		_OffsetPositionX = Mathf.RoundToInt((endPositionX - _StartPositionX) / _SizeX);
		_StartPositionX += Mathf.RoundToInt(_OffsetPositionX / 2);

		_OffsetPositionY = Mathf.RoundToInt((endPositionY - _StartPositionY) / _SizeY);
		_StartPositionY += Mathf.RoundToInt(_OffsetPositionY / 2);

		GD.Print("starting a grid");

		if(_backTexture != null)
		{
			Node2D backgroundNode = new Node2D();
			backgroundNode.Name = "Background grid";
			this.AddChild(backgroundNode);
			backgroundNode.Position = new Vector2((int)_AreaStartPoint2D.Position.x, (int)_AreaStartPoint2D.Position.y);

			for(int x = 0; x < _SizeX; x++)
			{
				for(int y = 0; y < _SizeY; y++)
				{
//					GD.Print("Creating a sprite");
					Sprite backSprite = new Sprite();
					backgroundNode.AddChild(backSprite, true);
					backSprite.Texture = _backTexture;
					backSprite.GlobalPosition = new Vector2( _StartPositionX + x * _OffsetPositionX, _StartPositionY + y * _OffsetPositionY);
					backSprite.Scale = new Vector2(_scaleOfTiles, _scaleOfTiles);

				}
			}

			_BackgroundContainerNode = backgroundNode;
		}

		_ForegroundContainerNode = new Node2D();
		_ForegroundContainerNode.Name = "Game sprites container";
		this.AddChild(_ForegroundContainerNode);
		if(_AreaStartPoint2D != null && AreaEndPoint2D != null)
			_ForegroundContainerNode.Position = new Vector2((int)_AreaStartPoint2D.Position.x, (int)_AreaStartPoint2D.Position.y);  
			
	}
	public override void _ExitTree()
	{
		GD.Print("I'm dying");
/*         if(_gameLogic != null)
			_gameLogic.DisplayGameFieldEvent -= DisplayGameField; */
	}

	public void ResetDisplay()
	{
		_BackgroundContainerNode.QueueFree();
		_ForegroundContainerNode.QueueFree();
/*         if(_gameLogic != null)
			_gameLogic.DisplayGameFieldEvent -= DisplayGameField; */



	}
	public void DisplayGameField(GameField gameField, float durationInSeconds)
	{

		//quick hack to test if changing offsets and start position will work for correct cells placement if the game field was resised in the process

		_StartPositionX = (int)_AreaStartPoint2D.GlobalPosition.x;
		_StartPositionY = (int)_AreaStartPoint2D.GlobalPosition.y;

		int endPositionX = (int)_AreaEndPoint2D.GlobalPosition.x;
		int endPositionY = (int)_AreaEndPoint2D.GlobalPosition.y;


		_OffsetPositionX = Mathf.RoundToInt((endPositionX - _StartPositionX) / _SizeX);
		_StartPositionX += Mathf.RoundToInt(_OffsetPositionX / 2);

		_OffsetPositionY = Mathf.RoundToInt((endPositionY - _StartPositionY) / _SizeY);
		_StartPositionY += Mathf.RoundToInt(_OffsetPositionY / 2);


		//list of cells from a gamefield

		//list of cells without "parents" on a gamefield
		//clear

		//update list of cells with parents and create new

		GD.Print("--------");
		GD.Print("Start of displaying sprites");

		List<int> listOfCellsFromAGameField = new List<int>();

		for(int x = 0; x < _SizeX; x++)
		{
			for(int y = 0; y < _SizeY; y++)
			{
				foreach(int cellId in gameField.GetCellsIdByCoords(x,y)) 
				{
					listOfCellsFromAGameField.Add(cellId);
					if(gameField.GetCellStatus(cellId) == Cell.Status.movedAndDeleted)
						GD.Print("Got a cell for deletion: " + cellId + " and value: " + gameField.GetCellValueById(cellId));
				}
			}
		}    


		List<int> listOfOrphanCells = new List<int>();
		foreach(int cellId in listOfCellsFromAGameField)
		{
			if(!gameField.CheckIfCellIdExists(cellId))
				listOfOrphanCells.Add(cellId);
		}

/*         foreach(int cellId in listOfOrphanCells)
		{
			_DisplayData[cellId].attachedSprite.QueueFree();
			_DisplayData.Remove(cellId);
			GD.Print("Deleted a node with id=" + cellId);
		}
 */

		 foreach(int cellId in listOfCellsFromAGameField)
		{
			int cellValue = gameField.GetCellValueById(cellId);
			int x = (int)gameField.GetCellCoords(cellId).x;
			int y = (int)gameField.GetCellCoords(cellId).y;

			Cell.Status cellStatus = gameField.GetCellStatus(cellId);

//            GD.Print("Got cell:" + cellId + " value: " + cellValue + "  status:" + cellStatus);
		}

		GD.Print("---");

		List<Node> listOfCellsForDeletion = new List<Node>();

		foreach(int cellId in listOfCellsFromAGameField)
		{
			int cellValue = gameField.GetCellValueById(cellId);
			int x = (int)gameField.GetCellCoords(cellId).x;
			int y = (int)gameField.GetCellCoords(cellId).y;

			Cell.Status cellStatus = gameField.GetCellStatus(cellId);

			GD.Print("cell:" + cellId + " value: " + cellValue + "  status:" + cellStatus);

			//check statuses and do accordingly:
			// move, move and delete, delete or change
 
			if(cellStatus == Cell.Status.appeared)
			{

				Node2D spriteToMake = InstancePrefabTile(
								_ValueTextureMatch[cellValue], 
								"id=" + cellId + ":value=" + cellValue + "  status: " + gameField.GetCellStatus(cellId), 
								new Vector2( _StartPositionX + x * _OffsetPositionX, _StartPositionY + y * _OffsetPositionY), 
								new Vector2(_scaleOfTiles, _scaleOfTiles),
								_ForegroundContainerNode
							);


				DisplayGridDataCell dataCell = new DisplayGridDataCell();
				dataCell.cellId = cellId;
				dataCell.value = cellValue;
				dataCell.attachedTilePrefab = spriteToMake;
				dataCell.x = x;
				dataCell.y = y;
				_DisplayData[cellId]=dataCell;                
			} 
			else if(cellStatus == Cell.Status.moved)
			{
//                GD.Print("Going to move cell id:" + cellId + "  value: " + cellValue + " and status:" + cellStatus + "  at: " + x + "," + y);
				_DisplayData[cellId].attachedTilePrefab.Name = "id=" + cellId + ":value=" + cellValue + "  status: " + gameField.GetCellStatus(cellId);

				Vector2 newPosition = new Vector2( _StartPositionX + x * _OffsetPositionX, _StartPositionY + y * _OffsetPositionY);
				//NodeUtilities.MoveNode2D(_DisplayData[cellId].attachedSprite, newPosition, durationInSeconds);
				UtilitiesThreadSafe.Instance.MoveNode2D(_DisplayData[cellId].attachedTilePrefab, newPosition, durationInSeconds);
//                _DisplayData[cellId].attachedSprite.Modulate = new Color(0,0,1);

				_DisplayData[cellId].x = x;
				_DisplayData[cellId].y = y;
				_DisplayData[cellId].value = cellValue;
				_DisplayData[cellId].cellId = cellId; 
			} 
			else if(cellStatus == Cell.Status.changed)
			{
//                GD.Print("Going to change cell id:" + cellId + "  value: " + cellValue + " and status:" + cellStatus + "  at: " + x + "," + y);

				//load corresponding prefab for a given cell's value
				Node2D spriteToMake = InstancePrefabTile(
								_ValueTextureMatch[cellValue], 
								"id=" + cellId + ":value=" + cellValue + "  status: " + gameField.GetCellStatus(cellId), 
								new Vector2( _StartPositionX + x * _OffsetPositionX, _StartPositionY + y * _OffsetPositionY), 
								new Vector2(_scaleOfTiles, _scaleOfTiles),
								_ForegroundContainerNode
							);


				// delete(I suppose with a corresponding animation) previous tile
				_DisplayData[cellId].attachedTilePrefab.QueueFree();
				//replace the link to a new one
				_DisplayData[cellId].attachedTilePrefab = spriteToMake;
				
				_DisplayData[cellId].x = x;
				_DisplayData[cellId].y = y;
				_DisplayData[cellId].value = cellValue;
				_DisplayData[cellId].cellId = cellId; 
			}            
			else if(cellStatus == Cell.Status.movedAndChanged)
			{
				Vector2 newPosition = new Vector2( _StartPositionX + x * _OffsetPositionX, _StartPositionY + y * _OffsetPositionY);

//                GD.Print("Going to move and change cell id:" + cellId + "  value: " + cellValue + " and status:" + cellStatus + "  at: " + x + "," + y);
				//load corresponding prefab for a given cell's value
				Node2D spriteToMake = InstancePrefabTile(
								_ValueTextureMatch[cellValue], 
								"id=" + cellId + ":value=" + cellValue + "  status: " + gameField.GetCellStatus(cellId), 
								newPosition, 
								new Vector2(_scaleOfTiles, _scaleOfTiles),
								_ForegroundContainerNode
							);

				// delete(I suppose with a corresponding animation) previous tile
				_DisplayData[cellId].attachedTilePrefab.QueueFree();
				//replace the link to a new one
				_DisplayData[cellId].attachedTilePrefab = spriteToMake;

//                NodeUtilities.MoveNode2D(_DisplayData[cellId].attachedSprite, newPosition, durationInSeconds);
				UtilitiesThreadSafe.Instance.MoveNode2D(_DisplayData[cellId].attachedTilePrefab, newPosition, durationInSeconds);

//                _DisplayData[cellId].attachedSprite.Texture = _ValueTextureMatch[cellValue];
//                _DisplayData[cellId].attachedSprite.Modulate = new Color(1,1,0);
				_DisplayData[cellId].x = x;
				_DisplayData[cellId].y = y;
				_DisplayData[cellId].value = cellValue;
				_DisplayData[cellId].cellId = cellId; 
			}  
			else if(cellStatus == Cell.Status.movedAndDeleted)
			{
				GD.Print("Going to delete cell id:" + cellId + "  value: " + cellValue + " and status:" + cellStatus + "  at: " + x + "," + y);
//                _DisplayData[cellId].attachedSprite.Modulate = new Color(1,0,0);
				if(_DisplayData.ContainsKey(cellId))
				{
					Vector2 newPosition = new Vector2( _StartPositionX + x * _OffsetPositionX, _StartPositionY + y * _OffsetPositionY);
					//NodeUtilities.MoveNode2D(_DisplayData[cellId].attachedSprite, newPosition, durationInSeconds);
					UtilitiesThreadSafe.Instance.MoveNode2D(_DisplayData[cellId].attachedTilePrefab, newPosition, durationInSeconds);
					
					listOfCellsForDeletion.Add(_DisplayData[cellId].attachedTilePrefab);
					_DisplayData.Remove(cellId);
					GD.Print("Deleted a node with id=" + cellId);
				}
			}
			else if(cellStatus == Cell.Status.deleted)
			{
				GD.Print("Going to delete cell id:" + cellId + "  value: " + cellValue + " and status:" + cellStatus + "  at: " + x + "," + y);
//                _DisplayData[cellId].attachedSprite.Modulate = new Color(1,0,0);
				if(_DisplayData.ContainsKey(cellId))
				{
					listOfCellsForDeletion.Add(_DisplayData[cellId].attachedTilePrefab);
					_DisplayData.Remove(cellId);
					GD.Print("Deleted a node with id=" + cellId);
				}
			} 
		} 

		//NodeUtilities.DeleteNodesDelayed(listOfCellsForDeletion, durationInSeconds);
		UtilitiesThreadSafe.Instance.DeleteNodesDelayed(listOfCellsForDeletion, durationInSeconds + .1f);

		if(_labelScoreToUse != null)
			_labelScoreToUse.Text = gameField.GetScore().ToString();
	}

	Node2D InstancePrefabTile(PackedScene prefabToInstance, string name, Vector2 globalPosition, Vector2 scale, Node2D parentNode)
{
		Node2D spriteToMake = (Node2D)GD.Load<PackedScene>(prefabToInstance.ResourcePath).Instance();

		spriteToMake.Name = name;
		parentNode.AddChild(spriteToMake);
		spriteToMake.GlobalPosition = globalPosition;
		spriteToMake.Scale = scale;

		return spriteToMake;
	}
}
