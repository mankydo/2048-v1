using Godot;
using System;

public class UIRootPassthrough : Node2D, IStorable, IGameController
{

    [Export]
    private NodePath _GameControllerNodePath;
    IGameController _GameController;

    [Export]
    private string[] _SceneNames;
    [Export]
    private NodePath[] _Scenes;


    [Signal]
    delegate void RestartGame(); 

    [Signal]
    delegate void NewGame();

    public override void _Ready()
    {
        if(_GameControllerNodePath != null) 
        {
            Type type = GetNode(_GameControllerNodePath).GetType().GetInterface(typeof(IGameController).Name);

            if(type != null)
            {
                _GameController = (IGameController)GetNode(_GameControllerNodePath);
            }
            else
                GD.Print("!!!See no Game Controller to use for.");

            GD.Print("got a GameController to operate: " + type.Name);
        } 
    }

    public void PassSignalToGameController(string signalName){
        _GameController.InvokeSignal(signalName);
    }

    public string GetParameter(string parameterName)
    {
        string result = "";

        if(_GameController != null)
        {
            GD.Print("got parameter to show: " + parameterName + " and it's value is: " + ((IStorable)_GameController).GetParameter(parameterName) + ";;;");
            result = ((IStorable)_GameController).GetParameter(parameterName);
        }

        return result;
    }

    public void InvokeSignal(string signalName)
    {
        EmitSignal(signalName); 
    }

    public void ShowMenu(string name)
    {
        int indexToUse = -1;
        for(int i = 0; i <= _SceneNames.Length; i++)
        {
            if(_SceneNames[i] == name)
            {
                indexToUse = i;
                break;
            }
        }

        if(indexToUse >= 0)
        {
            if(_Scenes[indexToUse] != null) 
            {
                Type type = GetNode(_Scenes[indexToUse]).GetType();

                if(type == typeof(StartMenuProxySignals))
                {
                    StartMenuProxySignals menuToShow = (StartMenuProxySignals)GetNode(_Scenes[indexToUse]);
                    menuToShow.ShowMenu();
                }
                else
                    GD.Print("no menu to show");
            } 
        }
    }
}
