using Godot;
using System;

public class ButtonsConnect : Node2D
{
    [Signal]
    delegate void ButtonPressed();

    public override void _Ready()
    {
        
    }

    public void EmitPress()
    {
        GD.Print("button pressed    ");
        EmitSignal("ButtonPressed");
    }

}
