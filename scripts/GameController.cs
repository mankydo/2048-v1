using Godot;
using System;
using System.Collections.Generic;

//using UtilitiesThreadSafe;

public class GameController: Node, IStorable, IGameController {
    
    //
    // the class listens to user input and activate appropriate action in GameLogic 
    // also it should control game state i.e. game is ongoing, game finished, etc
    // 
    [Export]
    private string _GameVersion = "0.7.2";
    [Export]
    private string _VersionParamaterName = "version";
    [Export]
    private string _ScoreParameterName = "score";

    [Export]
    NodePath GameLogicNodeToOperateOnPath;
    [Export]
    NodePath[] DisplayFieldNodeToOperateOnPath;

    public int gameScore{
        get {return _GameField.GetScore();}
    }

    [Export]
    private int _SizeX = 4;
    [Export]
    private int _SizeY = 4;
    [Export(PropertyHint.Range, "0,2")]
    private float _TransitionDurationInSeconds = .5f;
    [Export(PropertyHint.Range, "0,2")]
    private float _InputBlockingTimeInSeconds = .7f;
    private GameField _GameField;
    private IGameLogic _gameLogic;
    private List<IDisplayField> _displayFieldWorkers = new List<IDisplayField>();

    [Signal]
    delegate void RestartGameSignal();

    public delegate void DisplayGameField(GameField gameField, float durationInSeconds);
    public event DisplayGameField DisplayGameFieldEvent;

    private ulong _delayStartTimeInTicks = 0;
    private ulong _startDelayTime = 0;


    public override void _Ready()
    {

//        UtilitiesThreadSafe.Instance.Test();

        if(GameLogicNodeToOperateOnPath != null) 
        {
            Type type = GetNode(GameLogicNodeToOperateOnPath).GetType().GetInterface(typeof(IGameLogic).Name);

            if(type != null)
            {
                _GameField = new GameField(_SizeX, _SizeY);
                _gameLogic = (IGameLogic)GetNode(GameLogicNodeToOperateOnPath);
                _gameLogic.GameInit(_GameField);
            }
            else
                GD.Print("!!!See no Game Logic to use for.");

            GD.Print("got a GameLogic to operate: " + type.Name);
        } 

        if(DisplayFieldNodeToOperateOnPath.Length > 0) 
        {
            foreach (NodePath nodePath in DisplayFieldNodeToOperateOnPath)
            {
                if(!nodePath.IsEmpty())
                {
                    Type type = GetNode(nodePath).GetType().GetInterface(typeof(IDisplayField).Name);

                    if(type != null)
                        _displayFieldWorkers.Add((IDisplayField)GetNode(nodePath));
                    else
                        GD.Print("!!!See no Game Logic to use for.");
                
                    GD.Print("got a DisplayField to operate: " + type.Name);
                } else 
                {
                    GD.Print("not a valid entry");
                }
            }
        }

        if(_gameLogic != null && _displayFieldWorkers.Count > 0)
        {
            foreach(IDisplayField worker in _displayFieldWorkers)
            { 
                worker.Init(_gameLogic);
            }

            _gameLogic.NoRoomLeftOnAFieldEvent += RestartGame;
        }
   }

/*     public override void _Process(float delta)
    {
        
    } */

    public void NewGame()
    {
        if(_gameLogic !=null) 
        {
            _GameField.ClearField();
            _GameField.SetScore(0);

            foreach(IDisplayField worker in _displayFieldWorkers)
            { 
                DisplayGameFieldEvent -= worker.DisplayGameField;
                worker.ResetDisplay();
                worker.Init(_gameLogic);
                DisplayGameFieldEvent += worker.DisplayGameField;
            }

            _gameLogic.StartNewGame();
            DisplayGameFieldEvent?.Invoke(_GameField, _TransitionDurationInSeconds);

        }
    }

    private void RestartGame()
    {
        GD.Print("!!!!!!!!!!!!!!!!!!!!!!!!! GAME IS LOST!!!!");
        EmitSignal("RestartGameSignal");
    }

    public void GameInput(string direction)
    {

        if(OS.GetTicksMsec() - _delayStartTimeInTicks >= (ulong)(_InputBlockingTimeInSeconds * 1000))
            _delayStartTimeInTicks = 0;

        InputGameEvent eventToProceed = InputGameEvent.noswipe;
        if(direction == "left")
        {
            GD.Print("left is pressed");
            eventToProceed = InputGameEvent.left;
        }
        if(direction == "right")
        {
            GD.Print("right is pressed");
            eventToProceed = InputGameEvent.right;
        }
        if(direction == "up")
        {
            GD.Print("up is pressed");
            eventToProceed = InputGameEvent.up;
        }
        if(direction == "down")
        {
            GD.Print("down is pressed");
            eventToProceed = InputGameEvent.down;
        }

        if(_gameLogic !=null && eventToProceed != InputGameEvent.noswipe) 
        {
            if(_delayStartTimeInTicks == 0)
            {
                bool hasAnyCellMoved = false;
                _gameLogic.ProcessUserInput(eventToProceed, out hasAnyCellMoved);

                if(!hasAnyCellMoved)
                {
                    //some visual indication that there was no movement done
                }
                else
                {
                    DisplayGameFieldEvent?.Invoke(_GameField, _TransitionDurationInSeconds);
                    _gameLogic.CreateNewCell();
                    DisplayGameFieldEvent?.Invoke(_GameField, _TransitionDurationInSeconds);
                }

                _delayStartTimeInTicks = OS.GetTicksMsec();
            }            
            //should be a timeout for an animation to play
//            _gameLogic.CreateNewCell();

        }        
    }

    public string GetParameter(string name)
    {
        string result = "";
        if(name == _VersionParamaterName)
            result = _GameVersion;
        
        if(name == _ScoreParameterName)
            result = _GameField.GetScore().ToString();

        return result;
    }

    public void InvokeSignal(string signalName)
    {
        GD.Print("got a signal: " + signalName);
        
        if(signalName == "NewGame")
        {
            NewGame();
        } else 
        {
            GD.PrintErr("There is no such signal '" + signalName + "' to invoke in GameController");
        }
    }
}