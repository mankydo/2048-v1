using Godot;
using System;

public class ChangeNodeVisibility : Node2D
{
    [Export]
    private bool _InvertStates = false;

    [Export]
    private float _AlphaEnabled = 1;
    [Export]
    private float _AlphaDisabled = 0;

    bool _toggleState = true;

    public override void _Ready()
    {
        if(_InvertStates)
        {
            float tempAlpha = _AlphaEnabled;
            _AlphaEnabled = _AlphaDisabled;
            _AlphaDisabled = tempAlpha;
        }
        SetAlphaVisibilityEnabled();
    }

    public void ToggleAlphaVisibility()
    {
        float alpha = _toggleState ? _AlphaEnabled : _AlphaDisabled;

        this.Modulate = new Color(this.Modulate, alpha);
        _toggleState = !_toggleState;

        GD.Print("Toggle visibility");
    }

    public void SetAlphaVisibilityEnabled()
    {
       this.Modulate = new Color(this.Modulate, _AlphaEnabled); 
    }

    public void SetAlphaVisibilityDisabled()
    {
       this.Modulate = new Color(this.Modulate, _AlphaDisabled); 
    }
}
