using Godot;
using System;
using System.Collections.Generic;

namespace UtilitiesThreadSafeAll
{

    class MovingNodeContainer
    {
        public Node2D nodeToMove;
        public Vector2 positionToMoveFrom;
        public Vector2 positionToMoveTo;
        public float timeToMoveInSeconds;
        public ulong startTime;
    }

// For an godot autoload the filename should be the same as a class name(and a class should inherits from Node) 
    public class UtilitiesThreadSafe : Node
    {
        private string test1 = "";

        private Dictionary<Node, ulong> _ObjectsToDeleteInSeconds = new Dictionary<Node, ulong>();
        private List<MovingNodeContainer> _ObjectsToMove = new List<MovingNodeContainer>();
  
        private static UtilitiesThreadSafe instance = null;
 
        public static UtilitiesThreadSafe Instance {  
            get {  
                if (instance == null) {  
                    instance = new UtilitiesThreadSafe();
                    instance.Name = "utilities test ";
                    //Node n = instance.GetTree().Root;
                }  
                return instance;  
            }  
        }

        public override void _Ready()
        {
//            GetTree().Root.AddChild(instance);
            instance = this;
            test1 = "testing 111";
            GD.Print("starting singleton utilities");

        }

        public override void _Process(float delta)
        {
//            GD.Print("I'm progressing every frame!");
            for(int i = _ObjectsToMove.Count - 1; i >= 0; i--)
            {
                if(!MoveNode2D(_ObjectsToMove[i], Time.GetTicksMsec()))
                    _ObjectsToMove.RemoveAt(i);
            }

            List<Node> objsToDelete = GetListOfNodesForDeletionInSeconds(_ObjectsToDeleteInSeconds);
            if(objsToDelete.Count > 0)
            {
                foreach(Node obj in objsToDelete)
                {
                    _ObjectsToDeleteInSeconds.Remove(obj);
                    obj.QueueFree();
                }
            }


        }

        public void Test()
        {
            //test
            GD.Print("Testing singleton utilities: " + test1);
            //nodeToUse.CallDeferred("add_child", instance);
            //GetNode("Node2D").AddChild(instance);
        }

        private List<Node> GetListOfNodesForDeletionInSeconds(Dictionary<Node, ulong> objectsToDelete)
        {
            List<Node> objsToDelete = new List<Node>();

            foreach(KeyValuePair<Node, ulong>  objToDelete in objectsToDelete)
            {
                if(objToDelete.Value < Time.GetTicksMsec())
                    objsToDelete.Add(objToDelete.Key);
            }

            return objsToDelete;

        }

        public void MoveNode2D(Node2D nodeToMove, Vector2 positionToMoveTo, float timeToMoveInSeconds)
        {
            MovingNodeContainer objToMove = new MovingNodeContainer();
            objToMove.nodeToMove = nodeToMove;
            objToMove.positionToMoveFrom = nodeToMove.GlobalPosition;
            objToMove.positionToMoveTo = positionToMoveTo;
            objToMove.timeToMoveInSeconds = timeToMoveInSeconds;
            objToMove.startTime = Time.GetTicksMsec();

            GD.Print("Starting moving");
            _ObjectsToMove.Add(objToMove);
        }
        private bool MoveNode2D(MovingNodeContainer objToMove, ulong currentTime)
        {
//            GD.Print("i'm moving: " + objToMove.nodeToMove.Name);

            bool timeStillTicking = true;

            ulong totalTimeToMove = (ulong)(objToMove.timeToMoveInSeconds * 1000);
            ulong timeFromStart = currentTime - objToMove.startTime;

            Vector2 startPosition = objToMove.positionToMoveFrom;

            Vector2 currentPos;
            
            if((float)timeFromStart / totalTimeToMove < 1)
                currentPos = LerpVec2(startPosition, objToMove.positionToMoveTo, (float)timeFromStart / totalTimeToMove);
            else
                currentPos = objToMove.positionToMoveTo;

            objToMove.nodeToMove.GlobalPosition = currentPos;

            if( timeFromStart > totalTimeToMove)
                timeStillTicking = false;

            return timeStillTicking;
        }


        public void DeleteNodesDelayed(List<Node> listOfNodesForDeletion, float timeToWaitBeforeDeleteInSeconds)
        {
            
            foreach(Node nodeToDelete in listOfNodesForDeletion )
            {
                if(!_ObjectsToDeleteInSeconds.ContainsKey(nodeToDelete))
                    _ObjectsToDeleteInSeconds[nodeToDelete] = Time.GetTicksMsec() + (ulong)(timeToWaitBeforeDeleteInSeconds * 1000);
            }
        }
        public static Vector2 LerpVec2(Vector2 vec1, Vector2 vec2, float t)
        {
            float x = Mathf.Lerp(vec1.x, vec2.x, t);
            float y = Mathf.Lerp(vec1.y, vec2.y, t);

            return  new Vector2(x, y);
        } 
    }

} 