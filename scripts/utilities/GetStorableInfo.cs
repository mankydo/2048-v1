using Godot;
using System;

public class GetStorableInfo : Label
{

    [Export]
    NodePath GameControllerNodeToOperateOnPath;
    [Export]
    string _ParameterNameToGet = "";
    private IStorable _gameController;
    public override void _Ready()
    {
        if(GameControllerNodeToOperateOnPath != null) 
        {
            Type type = GetNode(GameControllerNodeToOperateOnPath).GetType().GetInterface(typeof(IStorable).Name);

            if(type != null)
                _gameController = (IStorable)GetNode(GameControllerNodeToOperateOnPath);
            else
                GD.Print("!!!See no Game Controller to use for labels to print on: " + GameControllerNodeToOperateOnPath);
        }

        if(_gameController != null)
        {
            this.Text = _gameController.GetParameter(_ParameterNameToGet);
        }       
    }

    public void GetAndSetInfo()
    {
        GD.Print("tring to get info");
        if(_gameController != null)
        {
            this.Text = _gameController.GetParameter(_ParameterNameToGet);
        } 
    }
}
