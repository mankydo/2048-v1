using Godot;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Utilities
{
    static class NodeUtilities
    {
        public static async void MoveNode2D(Node2D nodeToMove, Vector2 positionToMoveTo, float timeToMoveInSeconds)
        {

/*             var timer = new Timer();
            nodeToMove.AddChild(timer);
            timer.WaitTime = instanceInterval;
            timer.Start();
 */

            GD.Print("Starting moving");

            ulong startTimer = OS.GetTicksMsec();
            ulong totalTimeToMove = (ulong)(timeToMoveInSeconds * 1000);
            ulong delta = 0;

            Vector2 startPosition = nodeToMove.GlobalPosition;

            Vector2 currentPos;

            do
            {
/*                 var instance = scene.Instance() as KinematicBody2D;
                instance.Position = GetGlobalMousePosition();
                AddChild(instance); */

                //await ToSignal(timer, "timeout");

                delta = OS.GetTicksMsec() - startTimer;
                
                if((float)delta / totalTimeToMove < 1)
                    currentPos = LerpVec2(startPosition, positionToMoveTo, (float)delta / totalTimeToMove);
                else
                    currentPos = positionToMoveTo;

                nodeToMove.GlobalPosition = currentPos;

                await Task.Delay(8);

            } while( delta < totalTimeToMove);
        }

        public static async void DeleteNodesDelayed(List<Node> listOfNodesForDeletion, float timeToWaitBeforeDeleteInSeconds)
        {
            int timeToWait = (int)(timeToWaitBeforeDeleteInSeconds * 1000) + 10;
            await Task.Delay(timeToWait);

            foreach(Node nodeToDelete in listOfNodesForDeletion )
            {
                nodeToDelete.QueueFree();
            }
        }

        public static Vector2 LerpVec2(Vector2 vec1, Vector2 vec2, float t)
        {
            float x = Mathf.Lerp(vec1.x, vec2.x, t);
            float y = Mathf.Lerp(vec1.y, vec2.y, t);

            return  new Vector2(x, y);
        }
    }
}