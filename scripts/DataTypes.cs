using System.Collections.Generic;
using System.Linq;
using Godot;

public class Cell
{
    public Cell(int idToSet){
        id = idToSet;
        value = 0;
        x = 0;
        y = 0;
    }
    public int value;
    public int id{get;}


    public int x;
    public int y;
    public enum Status{appeared, deleted, moved, movedAndDeleted, changed, movedAndChanged, none};
    public Status status;
}

public class GameField
{
    private int _SizeX, _SizeY;

    private int _GameScore = 0;

    private int _nextIdToSet = 0;

    public int gameScore {
        get {return _GameScore;}
    }

    public int sizeX {
        get {return _SizeX;}
    }

    public int sizeY {
        get {return _SizeY;}
    }
    public GameField(int sizeX, int sizeY)
    {
        _SizeX = sizeX;
        _SizeY = sizeY;
    }
    Dictionary<int, Cell> _Cells = new Dictionary<int, Cell>();
    public int CreateCell(){
        _nextIdToSet++;
        _Cells.Add(_nextIdToSet, new Cell(_nextIdToSet));
        return _nextIdToSet;
    }
    public void SetScore(int scoreToSet)
    {
        _GameScore = scoreToSet;
    }

    public int GetScore()
    {
        return _GameScore;
    }
    public void ClearField()
    {
        _Cells.Clear();
    }

    public void DeleteCell(int id)
    {
        _Cells.Remove(id);
    }

    public void MarkAsDeletedNullCells()
    {
        foreach(KeyValuePair<int, Cell> cell in _Cells)
        {
            if(cell.Value.value == 0)
                cell.Value.status = Cell.Status.deleted;
        }
    }

    public void ClearDeletedCells()
    {
        var toRemove = _Cells.Where(cell => cell.Value.status == Cell.Status.deleted || cell.Value.status == Cell.Status.movedAndDeleted).ToList();
        foreach(KeyValuePair<int, Cell> cell in toRemove)
        {
            _Cells.Remove(cell.Key);
        }
    }

    public int GetNumberOfNotNullCells()
    {
        int numberOfNotNullCells = 0;

        for(int y = 0; y <= _SizeY - 1; y++)
        {
            for(int x = 0; x <= _SizeX - 1; x++)
            {
                int value = GetCellValueByCoords(x, y);
                if(value > 0)
                    numberOfNotNullCells++;
            }
        }
        return numberOfNotNullCells;
    }
    private Cell GetNotNullCellByCoords(int x, int y)
    {
        foreach (Cell c in _Cells.Values)
        {
            if (c.x == x && c.y == y && (c.status != Cell.Status.deleted || c.status != Cell.Status.movedAndDeleted))
                return c;
        }
        return null;
    }
    private Cell GetCellById(int id)
    {
        Cell cell;
        if (_Cells.TryGetValue(id, out cell))
        {
            return cell;
        } else 
        {
            return null;
        }
    }

    public int GetCellValueByCoords(int x, int y)
    {
        foreach (Cell c in _Cells.Values)
        {
            if (c.x == x && c.y == y && (c.status != Cell.Status.deleted || c.status != Cell.Status.movedAndDeleted)) 
                return c.value;
        }
        return -1;
    }

    public int GetNotNullCellIdByCoords(Vector2 coords)
    {
        foreach (Cell c in _Cells.Values)
        {
            if (c.x == (int)Mathf.RoundToInt(coords.x) && c.y == (int)Mathf.RoundToInt(coords.y)
                    && (c.status != Cell.Status.deleted || c.status != Cell.Status.movedAndDeleted))
                return c.id;
        }
        return -1;
    }

    public int GetNotNullCellIdByCoords(int x, int y)
    {
        foreach (Cell c in _Cells.Values)
        {
            if (c.x == x && c.y == y && (c.status != Cell.Status.deleted || c.status != Cell.Status.movedAndDeleted))
                return c.id;
        }
        return -1;
    }

    public List<int> GetCellsIdByCoords(int x, int y)
    {
        List<int> result = new List<int>();
        foreach (Cell c in _Cells.Values)
        {
            if (c.x == x && c.y == y)
                result.Add(c.id);
        }
        return result;
    }

    public List<int> GetNonDeletedCellsIdByCoords(int x, int y)
    {
        List<int> result = new List<int>();
        foreach (int cid in GetCellsIdByCoords(x, y))
        {
            if (GetCellStatus(cid) != Cell.Status.deleted && GetCellStatus(cid) != Cell.Status.movedAndDeleted)
                result.Add(cid);
        }
        return result;
    }
    public int GetCellValueById(int id)
    {
        Cell cell;
        if (_Cells.TryGetValue(id, out cell))
        {
            return cell.value;
        } else 
        {
            return -1;
        }
    }

    public bool CheckIfCellIdExists(int id)
    {
        Cell cell;
        if (_Cells.TryGetValue(id, out cell))
        {
            return true;
        } else 
        {
            return false;
        }        
    }

    public int GetCellValueByCoords(Vector2 coords)
    {
        foreach (Cell c in _Cells.Values)
        {
            if (c.x == (int)Mathf.RoundToInt(coords.x) && c.y == (int)Mathf.RoundToInt(coords.y))
                return c.value;
        }
        return -1;
    }
    public void SetCellValue(int id, int value)
    {
        GD.Print("tring to set cell with id: " + id + " to value:" + value);
        if(id != -1)
        {
            _Cells[id].value = value;
            _Cells[id].status = Cell.Status.changed;
        }
    }
    public void SetCellCoords(int id, int x, int y)
    {
        if(id != -1)
        {
            _Cells[id].x = x;
            _Cells[id].y = y;
            _Cells[id].status = Cell.Status.changed;
        }
    }
    public void SetCellCoords(int id, Vector2 coords)
    {
        if(id != -1)
        {       
            _Cells[id].x = (int)Mathf.RoundToInt(coords.x);
            _Cells[id].y = (int)Mathf.RoundToInt(coords.y);
            _Cells[id].status = Cell.Status.changed;
        }
    }
    public void SetCellStatus(int id, Cell.Status status)
    {
        if(id != -1)
        {
            _Cells[id].status = status;
        }
    }
    public Cell.Status GetCellStatus(int id)
    {
        Cell.Status result = Cell.Status.none; 
        if(GetCellValueById(id) != -1)
        {
            result = _Cells[id].status;
        }
        return result;
    }

    public Vector2 GetCellCoords(int id)
    {
        Vector2 result = new Vector2(-1,-1); 
        if(GetCellValueById(id) != -1)
        {
            result = new Vector2(_Cells[id].x, _Cells[id].y);
        }
        return result;
    }
}